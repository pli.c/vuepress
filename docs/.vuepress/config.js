module.exports = {
    title: 'Students ❤️ LabelGuide',
    description: 'How to label images for AI training',
    base: '/vuepress/',
    dest: 'public',

    themeConfig: {
      sidebar: [
        '/',
        '/guide/labels'
      ],
      sidebarDepth: 2,
    }
}
